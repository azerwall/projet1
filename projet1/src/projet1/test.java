/**
 * 
 */
package projet1;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Scanner;

/**
 * @author 77783
 *
 */
public class test {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		System.out.println("je suis le projet 1");
//		double resultat=Math.pow(2, 3);
//		System.out.println(resultat);
		
		m14();
	}
	
	static void m1()
	{
		
		System.out.println("je suis m1");
	}
	
	static void m2()
	{
		String str1="hello";
		String str2="toto";
		String str3="a bientot";
		String str4=str1+"\t"+str2+"\t"+str3; 
		System.out.println("je suis m2");
		System.out.println(str4);
	}
	
	static void m3()
	{
		int a;
		System.out.println();
		String str="hello";
		System.out.println(str);
	}
	
	static void m4(){
	int a = 10;
	a*=15;
	System.out.println(a);
	}
	
	static void m5(){
		Scanner clavier=new Scanner(System.in);
		
		System.out.println("saisir votre nom : ");
		String reponse = clavier.nextLine();
		System.out.println("Bonjour " + reponse);
		String str="toto";
		int a =10;
		String res = str+=a;
		System.out.println(res);
	}
	
	static void m6(){
		String str1 = "toto";
		String str2=str1.toUpperCase();
		String str3=str1.toLowerCase();
		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
	}
	
	static void m7(){
		int a = 10;
		int b = 15;
		
		if (a == b)
			System.out.println("a et b sont egaux");
		else
			System.out.println("a et b sont differents");
	}
	
	static void m8(){
		Scanner clavier= new Scanner(System.in);
		System.out.println("a ?");
		int a = clavier.nextInt();
		System.out.println("b ?");
		int b = clavier.nextInt();
		
		if (a == b)
			System.out.println("a et b sont egaux");
		else
			System.out.println("a et b sont differents");
	}
	
	static void m9(){
		Scanner clavier= new Scanner(System.in);
		System.out.println("a ?");
		int a = clavier.nextInt();
		System.out.println("b ?");
		int b = clavier.nextInt();
		
		if (a < b)
			System.out.println("a < b");
		else if (a > b)
			System.out.println("a > b");
		else
			System.out.println("a == b");
	}
	
	static void m10(){
		Scanner clavier = new Scanner(System.in);
		String str1 = "toto";
		System.out.println("str2 = ");
		String str2 = clavier.nextLine();
		
		//if (str1 == str2)
		//if (str1.equals(str2))
		if (str1.equalsIgnoreCase(str2))
			System.out.println("egaux");
		else
			System.out.println("different");
	}
	
	static void m11(){
		String str1 = "toto";
		String str2 = "toto";
		String res;
		
		res = (str1.equals(str2)) ? "egaux" : "diff";
		System.out.println(res);
	}
	
	static void m12(){
		int a = 20;
		int b =20;
		String  res;
		
		res = (a == b) ? "egaux" : "diff";
		System.out.println(res);
	}
	
	static void m13(){
		Scanner clavier= new Scanner(System.in);
		System.out.println("age ?");
		int age = clavier.nextInt();
		String res = "vous etes ";
		res+=(age >= 18) ? " majeur" : " mineur";
		System.out.println(res);
	}
	
	static void m14(){
		Scanner clavier= new Scanner(System.in);
		System.out.println("nb ?");
		int nb = clavier.nextInt();
		
		switch (nb){
		case 1 :
			System.out.println("lundi");
			break;
		case 2 :
			System.out.println("mardi");
			break;
		case 3 :
			System.out.println("mercredi");
			break;
		case 4 :
			System.out.println("jeudi");
			break;
		case 5 :
			System.out.println("vendredi");
			break;
		case 6 :
			System.out.println("samedi");
			break;
		case 7 :
			System.out.println("dimanche");
			break;
		default:
			System.out.println("erreur");
			break;
		}
	}
}
